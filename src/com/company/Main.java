package com.company;

import java.util.Scanner;
import java.util.Random;
import java.lang.String;

public class Main {

    public static void main(String[] args) {
        Scanner sn = new Scanner(System.in);
        Random rn = new Random();
        String[] Jobs = {"Warrior", "Thief", "Pirate", "Archer", "Magician"};
        String[] M_Jobs = {"Normal", "BOSS"};
        String Name = "";
        String M_Name = "";
        int HP = 0, MP = 0;
        int M_HP = 0, M_MP = 0;
        int Jobs_ID = 0;
        int Monster_ID = 0;
        //建立我方資訊
        System.out.print("請選擇要使用的職業(0 = 劍士, 1 = 盜賊, 2 = 海盜, 3 = 弓箭手, 4 = 法師)：");
        Jobs_ID = sn.nextInt();
        System.out.print("請輸入角色的HP：");
        HP = sn.nextInt();
        System.out.print("請輸入角色的MP：");
        MP = sn.nextInt();
        System.out.print("請輸入角色的名稱：");
        Name = sn.next();
        //建立怪物資訊
        System.out.print("\n請輸入怪物的類型(0 = 一般, 1 = BOSS)：");
        Monster_ID = sn.nextInt();
        M_HP = Monster_ID == 0 ? rn.nextInt(90) + 10 : rn.nextInt(900) + 600;
        M_MP = Monster_ID == 0 ? rn.nextInt(90) + 10 : rn.nextInt(900) + 600;

        System.out.println("\n即將進入戰鬥回合，雙方能力值：");
        System.out.println("我方：\n名稱：" + Name + "\n職業：" + Jobs[Jobs_ID] + "\nHP：" + HP + "\nMP：" + MP + "\n");

        String alphabet = "abcdefghijklmnopqrstuvwxyz";

        final int Name_Len = 5;
        StringBuilder sb = new StringBuilder();

        //隨機產生名字，透過亂數隨機抓取字串陣列位置
        for (int i = 0; i < Name_Len; i++)
            sb.append(alphabet.charAt(rn.nextInt(alphabet.length())));

        M_Name = sb.toString();

        System.out.println("對方：\n名稱：" + M_Name + "\n職業：" + M_Jobs[Jobs_ID] + "\nHP：" + M_HP + "\nMP：" + M_MP + "\n");

        //進入戰鬥回合，只要有一方生命<=0就停止結束
        while(HP > 0 && M_HP > 0)
        {
            boolean skill = true;
            int contin = 1;
            int chance = rn.nextInt(10) + 1;
            int damage;

            //假如機率為10，發動技能並扣除MP
            if(chance == 10 && contin == 1 && MP >= 20)
            {
                MP -= 20;
                //技能產生傷害範圍在30~10之間
                damage = rn.nextInt(20) + 10;
                M_HP -= damage;
                skill = true;
            }
            else
            {
                //一般產生傷害範圍在11~1之間
                damage = rn.nextInt(10) + 1;
                M_HP -= damage;
                skill = false;
            }

            boolean M_Skill = true;
            int M_chance = rn.nextInt(10) + 1;
            int M_damage = 0;

            if(M_chance == 10 && M_MP >= 15)
            {
                M_MP -= 15;
                M_damage = rn.nextInt(15) + 5;
                HP -= M_damage;
                M_Skill = true;
            }
            else
            {
                M_damage = rn.nextInt(7) + 1;
                HP -= M_damage;
                M_Skill = false;
            }

            System.out.print("我方損失了" + M_damage + "點HP/" + (skill == true ? 20: 0) + "點MP，剩餘HP：" + HP + "點/MP：" + MP +"點\n");
            System.out.println("對方損失了" + damage + "點HP/" + (M_Skill == true ? 15 : 0) + "點MP，剩餘HP：" + M_HP + "點/MP：" + M_MP +"點\n");

            if(HP > 0 && M_HP > 0) {
                System.out.print("我方是否繼續攻擊？(0 = 不攻擊, 1 = 攻擊)：");
                contin = sn.nextInt();
            }
        }

        //有一方生命<=0則為輸家
        if(M_HP <= 0)
            System.out.println("贏家是我方！");
        else
            System.out.println("贏家是怪物！");
    }
}
